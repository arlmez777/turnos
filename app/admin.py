from django.contrib import admin
from .models import *

@admin.register(Estado)
class EstadoAdmin(admin.ModelAdmin):
    list_display = ('descripcion', 'activo')

@admin.register(Tickets)
class TicketsAdmin(admin.ModelAdmin):
    model = Tickets
    list_display = ('numero_ticket', 'descripcion', 'estado', 'prioridades', 'fecha', 'hora')

@admin.register(Usuario)
class UsuarioAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'apellido')

@admin.register(Permisos)
class PermisosAdmin(admin.ModelAdmin):
    list_display = ['descripcion']